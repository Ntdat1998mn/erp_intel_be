const express = require("express");
const {
  getFolderList,
  getFolderByFolderId,
  createFolder,
  deleteFolder,
  updateFolder,
} = require("../controller/folderController");

const folderRoute = express.Router();

folderRoute.get("/:id", getFolderByFolderId);
folderRoute.get("", getFolderList);
folderRoute.post("", createFolder);
folderRoute.put("/:id", updateFolder);
folderRoute.delete("/:id", deleteFolder);

module.exports = folderRoute;
