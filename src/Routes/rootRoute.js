const express = require("express");
const authRoute = require("./authRoute");
const folderRoute = require("./folderRoute");
const fileRoute = require("./fileRoute");
const { verifyToken } = require("../utils/verifyToken");

const rootRoute = express.Router();

rootRoute.use("/auth", authRoute);
rootRoute.use("/folder", verifyToken, folderRoute);
rootRoute.use("/file", verifyToken, fileRoute);

/* rootRoute.use("/user", userRoute); */

/* rootRoute.use("/work-detail", workDetailRoute); */

module.exports = rootRoute;
