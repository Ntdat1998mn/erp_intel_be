const express = require("express");
const { logIn } = require("../controller/authController");

const authRoute = express.Router();

authRoute.post("/logIn", logIn);

module.exports = authRoute;
