const express = require("express");
const {
  getFileList,
  getFileByFileId,
  createFile,
  deleteFile,
} = require("../controller/fileController");
const { upload } = require("../utils/upload");

const fileRoute = express.Router();

fileRoute.get("/:id", getFileByFileId);
fileRoute.get("", getFileList);
fileRoute.post("", upload.single("file"), createFile);
fileRoute.delete("/:id", deleteFile);

module.exports = fileRoute;
