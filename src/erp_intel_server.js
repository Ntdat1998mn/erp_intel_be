const express = require("express");
const app = express();
app.use(express.json());

app.use(express.static("."));

const cors = require("cors");
app.use(cors());

app.listen(3004);
const rootRoute = require("./Routes/rootRoute");
app.use("/api/intel", rootRoute);
