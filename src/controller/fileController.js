const { PrismaClient } = require("@prisma/client");
const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
  failAuthenticationCode,
} = require("../config/response");
const prisma = new PrismaClient();

// Lấy danh sách tệp
const getFileList = async (req, res) => {
  try {
    let data = await prisma.tep.findMany({
      include: {
        trao_doi: true,
        /*  hr_user: {
          select: {
            user_id: true,
            user_fullname: true,
          },
        }, */
      },
    });
    if (data.length > 0) {
      successCode(res, data, "Lấy danh sách tệp thành công!");
    } else {
      notFoundCode(res, "Không có tệp nào!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy tệp theo id tệp
const getFileByFileId = async (req, res) => {
  try {
    let tep_id = req.params.id * 1;
    let data = await prisma.tep.findFirst({
      where: { tep_id },
    });
    if (data) {
      successCode(res, data, "Lấy tệp thành công!");
    } else notFoundCode(res, "Không tìm thấy dữ liệu!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Tạo tệp
const createFile = async (req, res) => {
  try {
    const file = req.file;
    if (!file) {
      failCode(res, "Không có tệp nào được tải lên!");
    }
    const user = req.user.content;
    const dataUser = await prisma.hr_user.findFirst({
      where: { user_id: user.user_id },
    });
    if (!dataUser) {
      notFoundCode(res, "Người dùng không tồn tại!");
    }
    const fileInfor = JSON.parse(req.body.fileInfor);
    const dataFolder = await prisma.thu_muc.findFirst({
      where: { thu_muc_id: fileInfor.thu_muc_id },
    });
    if (!dataFolder) {
      notFoundCode(res, "Thư mục không tồn tại không tồn tại!");
    }
    const baseUrl = `${req.protocol}://${req.get("host")}`;
    const data = {
      ...fileInfor,
      nguoi_tao_tep: user.user_id,
      link_tep: `${req.file.destination}/${req.file.filename}`,
    };
    let newFile = await prisma.tep.create({ data });
    newFile.link_tep = baseUrl + "/" + newFile.link_tep;
    successCode(res, newFile, "Thêm tệp thành công!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Xoá tệp
const deleteFile = async (req, res) => {
  try {
    const tep_id = req.params.id * 1;
    const File = await prisma.tep.findFirst({
      where: { tep_id },
    });
    if (!File | (File.trang_thai_tep === 0)) {
      notFoundCode(res, "Không tìm thấy tệp!");
    }
    const user = req.user.content;
    if (File.nguoi_tao_tep !== user.user_id) {
      failAuthenticationCode(res, "Người dùng này không có quyền xoá tệp!");
    }
    const deletedFile = await prisma.tep.update({
      data: { trang_thai_tep: 0 },
      where: { tep_id },
    });
    successCode(res, deletedFile, "Xoá tệp thành công!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  getFileList,
  getFileByFileId,
  createFile,
  deleteFile,
};
