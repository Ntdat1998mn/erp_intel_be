const { PrismaClient } = require("@prisma/client");
const bcrypt = require("bcryptjs");
const { createToken } = require("../utils/verifyToken");
const { successCode, errorCode, failCode } = require("../config/response");
const prisma = new PrismaClient();

/* //Đăng ký
const signUp = async (req, res) => {
  try {
    let {
      name,
      email,
      password,
      age,
      phone,
      birthday,
      gender,
      skill,
      certification,
    } = req.body;
    let model = {
      name,
      email,
      password: bcrypt.hashSync(password, 10),
      age,
      phone,
      birthday,
      gender,
      skill,
      certification,
    };
    let checkEmail = await prisma.NguoiDung.findFirst({ where: { email } });
    if (checkEmail) {
      model.password = "";
      failCode(res, "Email đã tồn tại!");
    } else {
      let data = await prisma.NguoiDung.create({ data: model });
      console.log("data: ", data);
      data.password = "";
      successCode(res, data, "Đăng ký thành công!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
}; */

// Đăng nhập
const logIn = async (req, res) => {
  try {
    let { user_username, user_password } = req.body;
    let data = await prisma.hr_user.findFirst({
      where: { user_username },
    });
    if (data) {
      if (user_password === data.user_password) {
        /* let checkPassword = bcrypt.compareSync(user_password, data.user_password);
      if (checkPassword) {*/
        let token = createToken(data);
        data.user_password = "";
        successCode(res, { ...data, token }, "Đăng nhập thành công!");
      } else {
        user_password = "";
        failCode(res, "Mật khẩu không đúng!");
      }
    } else {
      failCode(res, "Tài khoản không tồn tại!");
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = { logIn };
