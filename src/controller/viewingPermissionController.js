const { PrismaClient } = require("@prisma/client");
const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
  failAuthenticationCode,
} = require("../config/response");
const prisma = new PrismaClient();

// Lấy quyền xem theo id thư mục
const getFolderByFolderId = async (req, res) => {
  try {
    let thu_muc_id = req.params.id * 1;
    let data = await prisma.thu_muc.findFirst({
      where: { thu_muc_id },

      include: {
        hr_user: {
          select: {
            user_id: true,
            user_fullname: true,
          },
        },
        tep: true,
        quyen_xem: {
          select: {
            hr_user: {
              select: {
                user_id: true,
                user_fullname: true,
              },
            },
          },
        },
      },
    });
    if (data) {
      const baseUrl = `${req.protocol}://${req.get("host")}`;
      data.tep.forEach((file) => {
        file.link_tep = baseUrl + "/" + file.link_tep;
      });
      successCode(res, data, "Lấy thư mục thành công!");
    } else failCode(res, "Không tìm thấy dữ liệu!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Xoá người có quyền xem trong thư mục

// Thêm người xem vào thư mục
