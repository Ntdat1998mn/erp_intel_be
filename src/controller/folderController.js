const { PrismaClient } = require("@prisma/client");
const {
  successCode,
  errorCode,
  failCode,
  notFoundCode,
  failAuthenticationCode,
} = require("../config/response");
const prisma = new PrismaClient();

// Lấy danh sách thư mục
const getFolderList = async (req, res) => {
  try {
    let data = await prisma.thu_muc.findMany({
      include: {
        hr_user: {
          select: {
            user_id: true,
            user_fullname: true,
          },
        },
        tep: true,
        quyen_xem: {
          select: {
            hr_user: {
              select: {
                user_id: true,
                user_fullname: true,
              },
            },
          },
        },
      },
    });
    if (data.length > 0) {
      const baseUrl = `${req.protocol}://${req.get("host")}`;
      data.forEach((folder) => {
        folder.tep.forEach((file) => {
          file.link_tep = baseUrl + "/" + file.link_tep;
        });
      });
      successCode(res, data, "Lấy danh sách thư mục thành công!");
    } else {
      notFoundCode(res, "Không có thư mục nào!");
      return;
    }
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Lấy thư mục theo id thư mục
const getFolderByFolderId = async (req, res) => {
  try {
    let thu_muc_id = req.params.id * 1;
    let data = await prisma.thu_muc.findFirst({
      where: { thu_muc_id },

      include: {
        hr_user: {
          select: {
            user_id: true,
            user_fullname: true,
          },
        },
        tep: true,
        quyen_xem: {
          select: {
            hr_user: {
              select: {
                user_id: true,
                user_fullname: true,
              },
            },
          },
        },
      },
    });
    if (data) {
      const baseUrl = `${req.protocol}://${req.get("host")}`;
      data.tep.forEach((file) => {
        file.link_tep = baseUrl + "/" + file.link_tep;
      });
      successCode(res, data, "Lấy thư mục thành công!");
    } else failCode(res, "Không tìm thấy dữ liệu!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Tạo thư mục
const createFolder = async (req, res) => {
  try {
    const user = req.user.content;
    const dataUser = await prisma.hr_user.findFirst({
      where: { user_id: user.user_id },
    });
    if (!dataUser) {
      failCode(res, "Người dùng không tồn tại!");
      return;
    }
    const data = { ...req.body, nguoi_tao_thu_muc: user.user_id };
    let newFolder = await prisma.thu_muc.create({ data });
    successCode(res, newFolder, "Thêm thư mục thành công!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Cập nhật thư mục
const updateFolder = async (req, res) => {
  try {
    const thu_muc_id = req.params.id * 1;
    const data = req.body;
    const folder = await prisma.thu_muc.findFirst({
      where: { thu_muc_id },
    });
    if (!folder) {
      notFoundCode(res, "Không tìm thấy thư mục!");
      return;
    }
    const user = req.user.content;
    if (folder.nguoi_tao_thu_muc !== user.user_id) {
      failAuthenticationCode(
        res,
        "Người dùng này không được phép cập nhật thư mục!"
      );
      return;
    }
    let updatedFolder = await prisma.thu_muc.update({
      data,
      where: { thu_muc_id },
    });
    successCode(res, updatedFolder, "Cập nhật thư mục thành công!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

// Xoá thư mục
const deleteFolder = async (req, res) => {
  try {
    const thu_muc_id = req.params.id * 1;
    const folder = await prisma.thu_muc.findFirst({
      where: { thu_muc_id },
    });
    if (!folder | (folder.trang_thai_thu_muc === 0)) {
      notFoundCode(res, "Không tìm thấy thư mục!");
      return;
    }
    const user = req.user.content;
    if (folder.nguoi_tao_thu_muc !== user.user_id) {
      failAuthenticationCode(res, "Người dùng này không có quyền xoá thư mục!");
      return;
    }
    const deletedFolder = await prisma.thu_muc.update({
      data: { trang_thai_thu_muc: 0 },
      where: { thu_muc_id },
    });
    successCode(res, deletedFolder, "Xoá thư mục thành công!");
  } catch (err) {
    errorCode(res, err.message);
  }
};

module.exports = {
  getFolderList,
  getFolderByFolderId,
  createFolder,
  deleteFolder,
  updateFolder,
};
